package se.rsv.hack.api;

import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import se.rsv.hack.Greeting;


@RestController
@RequestMapping("/api")
public class GreetingApiController {

	private static Logger log = LoggerFactory.getLogger(GreetingApiController.class);
	
	private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();	
	
    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        log.debug("greeting called with name = {}", name);
    	model.addAttribute("name", name);
    	return new Greeting(counter.incrementAndGet(),
                String.format(template, name));
    }
}
