package se.rsv.hack.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

	private static Logger log = LoggerFactory.getLogger(GreetingController.class);
		
    @RequestMapping("/greeting")
    public String greeting(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        log.debug("greeting called with name = {}", name);
    	model.addAttribute("name", name);
        return "greeting";
    }

    
}
